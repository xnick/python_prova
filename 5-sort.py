#!/usr/bin/python
#Objecte
#-----------------------------------------------
import sys
import argparse
# Processar els arguments amb argparse
parser = argparse.ArgumentParser(description='mostrar linies d\'un fitxer')
parser.add_argument('-f','--file',dest='fileIn', help='fitxer a processar, default=stdin', type=file,default="/dev/stdin")
parser.add_argument('-s',dest='xsort',help='Camp per ordenar',type=str,default='login',choices=['login','uid','gname'])
args=parser.parse_args()
print args
class UnixUsuari():
    "Classe Unix usuari : fitxer /etc/passwd"
    def __init__(self,userLine):
        userFields=userLine.split(":")
        self.login=userFields[0]
        self.password=userFields[1]
        self.uid=userFields[2]
        self.gid=userFields[3]
        self.gecos=userFields[4]
        self.directory=userFields[5]
        self.shell=userFields[6]
    def getGecos(self):
        "retorn el valor de gecos"
        return self.gecos
    def putGecos(self,newGecos):
        "estableix el valor de gecos"
        self.gecos=newGecos
    def mini_print(self):
        "mostra login, uid, gecos"
        return "%s %s %s" % (self.login,self.uid,self.gecos)
    def __str__(self):
        return '%s:%s:%s:%s' % (self.login,self.uid,self.gecos,self.shell)

def cmp_login(a,b):
    "comparador d'usuaris per login"
    if a.login > b.login:   return 1
    if a.login < b.login:   return -1
    return 0
def cmp_uid(a,b):
    "comparador d'usuaris per uid"
    if int(a.uid) > int(b.uid):     return 1
    if int(a.uid) < int(b.uid):     return -1
    if a.login > b.login:   return 1
    if a.login < b.login:   return -1
    return 0
def cmp_gid(a,b):
    "comparador d'usuaris per gid"
    if int(a.gid) > int(b.gid):     return 1
    if int(a.gid) < int(b.gid):     return -1
    if a.login > b.login:   return 1
    if a.login < b.login:   return -1
    return 0

listUsers=[]
for e in args.fileIn:
    listUsers.append(UnixUsuari(e[:-1]))
args.fileIn.close()
if args.xsort == "uid":
    listUsers.sort(cmp=cmp_uid)
elif args.xsort == "gname":
    listUsers.sort(cmp=cmp_gid)
else:
    listUsers.sort(cmp=cmp_login)

for oneUser in listUsers:
    print "%-20s   %-20s  %-20s" % (oneUser.login,oneUser.uid,oneUser.gid)

