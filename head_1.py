#!/usr/bin/python
#Head 1
#----------------------------------------------------
import sys
NUMLIN=5
fileIn=sys.stdin
if len(sys.argv) == 2:
    fileIn=open(sys.argv[1],'r')
lineCounter=0
for line in fileIn:
    lineCounter+=1
    print line
    if lineCounter == NUMLIN: break
fileIn.close()
sys.exit(0)
