#!/usr/bin/python
#Tail
#-----------------------------------------------
import sys
NUMLIN=5
fileIn=sys.stdin
if len(sys.argv) == 2:
    fileIn =open(sys.argv[1],'r')
xlines=[]
for e in fileIn:
    xlines.append(e[:-1])
    if len(xlines) == NUMLIN: break
for xe in reversed(xlines):
    print xe
fileIn.close()
sys.exit(0)
